package com.kalys.axonspringtut.aggregates

import com.kalys.axonspringtut.coreapi.*
import org.axonframework.commandhandling.CommandHandler
import org.axonframework.eventsourcing.EventSourcingHandler
import org.axonframework.modelling.command.AggregateIdentifier
import org.axonframework.modelling.command.AggregateLifecycle
import org.axonframework.spring.stereotype.Aggregate
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*


@Aggregate
class Order {

    @AggregateIdentifier
    private lateinit var orderId: UUID
    private var orderConfirmed: Boolean = false
    private lateinit var product: String
    private var shiped: Boolean = false

    private val logger: Logger = LoggerFactory.getLogger(Order::class.java)
    private var count: Int = 0

    private constructor()

    @CommandHandler
    constructor(placeOrderCommand: PlaceOrderCommand) : this() {
        AggregateLifecycle.apply(OrderPlacedEvent(placeOrderCommand.orderId, placeOrderCommand.product))
    }

    @EventSourcingHandler
    fun on(placedEvent: OrderPlacedEvent) {
        this.orderId = placedEvent.orderId
        this.product = placedEvent.product
        logger.info("Order with id $orderId is placed ${++count}")
    }

    @CommandHandler
    fun handle(confirmOrderCommand: ConfirmOrderCommand) {
        AggregateLifecycle.apply(OrderConfirmedEvent(orderId))
        logger.info("Order with id $orderId is confirmed")
    }

    @CommandHandler
    fun handle(command: ShipOrderCommand) {
        if (!orderConfirmed) {
            throw Exception()
        }
        AggregateLifecycle.apply(OrderShippedEvent(orderId))
    }

    @EventSourcingHandler
    fun on(confirmedEvent: OrderConfirmedEvent) {
        this.orderConfirmed = true
    }

    @EventSourcingHandler
    fun handle(orderShippedEvent: OrderShippedEvent){
        this.shiped = true
    }
}