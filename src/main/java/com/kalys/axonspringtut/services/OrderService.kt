package com.kalys.axonspringtut.services

import com.kalys.axonspringtut.coreapi.FindOrderQuery
import com.kalys.axonspringtut.coreapi.PlaceOrderCommand
import com.kalys.axonspringtut.query.OrderView
import org.axonframework.commandhandling.gateway.CommandGateway
import org.axonframework.messaging.responsetypes.ResponseTypes
import org.axonframework.queryhandling.QueryGateway
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*
import java.util.concurrent.CompletableFuture

@Service
class OrderService @Autowired constructor(
        private val commandGateway: CommandGateway,
        private val queryGateway: QueryGateway
) {

    private val logger: Logger = LoggerFactory.getLogger(OrderService::class.java)

    fun orderPlace(product: String): CompletableFuture<String> {
        println("order Placed")
        return commandGateway.send(PlaceOrderCommand(UUID.randomUUID(), product))
    }

    fun findOrder(orderId: UUID): OrderView {
        return queryGateway.query(
                FindOrderQuery(orderId),
                ResponseTypes.instanceOf(OrderView::class.java)
        ).get()
    }

}