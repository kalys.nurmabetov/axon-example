package com.kalys.axonspringtut.query

import com.kalys.axonspringtut.coreapi.FindOrderQuery
import com.kalys.axonspringtut.coreapi.OrderConfirmedEvent
import com.kalys.axonspringtut.coreapi.OrderPlacedEvent
import org.axonframework.eventhandling.EventHandler
import org.axonframework.queryhandling.QueryHandler
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

@Service
class OrderProjector (private val orderJPARepository: OrderJPARepository) {

    @EventHandler
    fun on(orderPlacedEvent: OrderPlacedEvent) {
        val orderView = OrderView(orderPlacedEvent.orderId, orderPlacedEvent.product)
        orderJPARepository.save(orderView)
    }

    @EventHandler
    fun on(orderConfirmedEvent: OrderConfirmedEvent) {
        orderJPARepository.findById(orderConfirmedEvent.orderId).ifPresent(OrderView::confirm)
    }

    @QueryHandler
    fun handle(findOrderQuery: FindOrderQuery): OrderView {
        return orderJPARepository.findById(findOrderQuery.orderId)
                .get()
    }
}