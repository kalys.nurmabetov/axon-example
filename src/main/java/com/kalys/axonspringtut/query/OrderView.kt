package com.kalys.axonspringtut.query

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id

@Entity
data class OrderView(
        @Id val orderId: UUID,
        var product: String,
        var orderConfirmed: Boolean = false
) {

    fun addProduct(product: String) {
        this.product = product
    }

    fun removeProduct() {
        this.product = ""
    }

    fun confirm() {
        this.orderConfirmed = true
    }
}


@Repository interface OrderJPARepository : JpaRepository<OrderView, UUID>

