package com.kalys.axonspringtut.coreapi

import org.axonframework.modelling.command.TargetAggregateIdentifier
import java.util.*

data class PlaceOrderCommand(
        @TargetAggregateIdentifier val orderId: UUID,
        val product: String
)

data class ConfirmOrderCommand(
        @TargetAggregateIdentifier val orderId: UUID
)

data class ShipOrderCommand(
        @TargetAggregateIdentifier val orderId: UUID
)


