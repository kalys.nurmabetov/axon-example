package com.kalys.axonspringtut.coreapi

import java.util.*

data class FindOrderQuery(val orderId: UUID)