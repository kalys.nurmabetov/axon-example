package com.kalys.axonspringtut.coreapi

import java.util.*

data class OrderPlacedEvent(
        val orderId: UUID,
        val product: String
)

data class OrderConfirmedEvent(
        val orderId: UUID
)

data class OrderShippedEvent(
        val orderId: UUID
)