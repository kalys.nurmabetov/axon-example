package com.kalys.axonspringtut.controllers

import com.kalys.axonspringtut.query.OrderView
import com.kalys.axonspringtut.services.OrderService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.*
import java.util.*


@RequestMapping("/orders")
@RestController
open class OrderController @Autowired constructor(private val orderService: OrderService) {

    @PostMapping("/place/{product}")
    fun placeOrder(@PathVariable product: String) {
        this.orderService.orderPlace(product)
    }

    @GetMapping("/{orderId}")
    fun getOrder(@PathVariable orderId: String): OrderView {
        return orderService.findOrder(UUID.fromString(orderId))
    }
}