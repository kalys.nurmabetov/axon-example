package com.kalys.axonspringtut;

import org.apache.catalina.core.ApplicationContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class AxonSpringTutApplication {

    public static void main(String[] args) {
        SpringApplication.run(AxonSpringTutApplication.class, args);
    }

}
