package com.kalys.axonspringtut

import com.kalys.axonspringtut.aggregates.Order
import com.kalys.axonspringtut.coreapi.OrderPlacedEvent
import com.kalys.axonspringtut.coreapi.PlaceOrderCommand
import org.axonframework.test.aggregate.AggregateTestFixture
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test
import java.util.*


class OrderTest : AbstractTestNGSpringContextTests() {

    private lateinit var fixture: AggregateTestFixture<Order>


    @BeforeClass
    fun setUp() {
        fixture = AggregateTestFixture(Order::class.java)
    }

    @Test
    fun `test placing of order`() {
        val orderId = UUID.randomUUID()
        val product = "Samsung TV"
        fixture.givenNoPriorActivity()
                .`when`(PlaceOrderCommand(orderId, product))
                .expectEvents(OrderPlacedEvent(orderId, product))
    }
}